defmodule ListFilter do
  require Integer

  def call(list) do
    list
    |> Enum.flat_map(fn each ->
      case Integer.parse(each) do
        {int, _rest} -> is_it_odd(int)
        :error -> []
      end
    end)
    |> Enum.count()
  end

  def is_it_odd(value) do
    case Integer.is_odd(value) do
      true -> [value]
      false -> []
    end
  end
end
